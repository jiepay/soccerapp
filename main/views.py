from django.shortcuts import render, redirect
from django.http import HttpResponse

from .models import Team, Match, Game, Person
from .forms import UserRegisterForm, GameForm
import datetime
from django.utils import timezone

# Create your views here.

def index(request):
    if request.session.get('user', None) is not None:
        return redirect('/schedule')

    form = UserRegisterForm
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            #register the user to the database
            query = Person.objects.filter(phoneNumber=form.cleaned_data['phoneNumber'])
            user = None
            if len(query) > 0:
                user = query[0]
                user.nickname = form.cleaned_data['nickname']
                user.save()
            else:
                user = Person.objects.create(nickname = form.cleaned_data['nickname'], phoneNumber=form.cleaned_data['phoneNumber'])

            request.session['user'] = user.id
            #redirect to the matchschedule page
            return redirect('/schedule')
        else:
            form = UserRegisterForm()
            
    return render(request, 'main/index.html', {'session_user': None, 'form':form})

def schedule(request):
    if request.session['user'] is None:
        return redirect('/')

    matches = Match.objects.filter(datetime__date=datetime.date.today()).filter(datetime__gt=timezone.now()).order_by('datetime')
    
    betable = []
    for match in matches:
        betable.append(len(Game.objects.filter(match=match).filter(user=request.session['user'])) == 0)

    return  render(
        request,
        'main/matchschedule.html',
        context = {'session_user': request.session['user'], 'matches': matches, 'betable':betable,}
        )

def matchscore(request, match_id):
    if request.session['user'] is None:
        return redirect('/')

    form = GameForm
    #session user
    sessionUser = Person.objects.get(id=request.session['user'])
    matchFromDb = Match.objects.get(id=match_id)
    #check if user and match exists already
    exist = Game.objects.filter(user=sessionUser).filter(match=match_id)
    played = matchFromDb.datetime < timezone.now()

    if len(exist) > 0 or played:
        return redirect('/schedule')

    if request.method == 'POST':
        form = GameForm(request.POST)
        if form.is_valid():
            #register user bet to the database

            game = Game.objects.create(
                user = sessionUser,
                match = matchFromDb,
                firstHalfTeam1Score = form.cleaned_data['firstHalfTeam1Score'],
                firstHalfTeam2Score = form.cleaned_data['firstHalfTeam2Score'],
                fullTimeTeam1Score = form.cleaned_data['fullTimeTeam1Score'],
                fullTimeTeam2Score = form.cleaned_data['fullTimeTeam2Score'],
                dateTimeScoreCreated = datetime.datetime.now()
                )
            #redirect to the matchschedule page
            return redirect('/thankyou')
        else:
            print("invalid form data")

    return render(
        request,
        'main/matchscore.html',
        {'session_user': request.session['user'], 'form': form, 'match': matchFromDb, 'user': sessionUser},
    )   


def winner(request):
    if(request.user is None):
        #redirect to the matchschedule page
        return redirect('/schedule')

    winnerList = []
    for match in Match.objects.filter(datetime__date__lte=datetime.date.today()).order_by('-datetime'):
        matchWinners = []
        for game in match.games.all():
            if (game.firstHalfTeam1Score == match.firstHalfTeam1Score and game.firstHalfTeam2Score == match.firstHalfTeam2Score 
                and game.fullTimeTeam1Score == match.fullTimeTeam1Score and game.fullTimeTeam2Score == match.fullTimeTeam2Score):
                matchWinners.append(game)
        if len(matchWinners) == 0:
            for game in match.games.all():
                if game.fullTimeTeam1Score == match.fullTimeTeam1Score and game.fullTimeTeam2Score == match.fullTimeTeam2Score:
                    matchWinners.append(game)
        winnerList.append({'match': match, 'winners': matchWinners})


    return  render(
        request,
        'main/winners.html',
        context = {'session_user': None, 'winners': winnerList}
    )

def thankyou(request):
    return render(
        request,
        'main/thankyou.html',
        {'session_user': request.session['user']}
    )

def logout(request):
    if request.method == 'POST':
        del request.session['user']
    return redirect('/')
