from django.db import models
import datetime

# Create your models here.
class Person(models.Model):
    firstName = models.CharField(max_length=50)
    lastName = models.CharField(max_length=50)
    nickname = models.CharField(max_length=20)
    phoneNumber = models.CharField(max_length=30)
    def __str__(self):
        return self.nickname + " with phone: " + self.phoneNumber

class Team(models.Model):
    name = models.CharField(max_length=50)
    countryCode = models.CharField(max_length=10)
    def  __str__(self):
        return self.name

class Match(models.Model):
    team1 = models.ForeignKey("Team",related_name="matches_home", on_delete=models.CASCADE)
    team2 = models.ForeignKey("Team",related_name="matches_away", on_delete=models.CASCADE)
    firstHalfTeam1Score = models.IntegerField(null=True, blank=True)
    firstHalfTeam2Score = models.IntegerField(null=True, blank=True)
    fullTimeTeam1Score = models.IntegerField(null=True, blank=True)
    fullTimeTeam2Score = models.IntegerField(null=True, blank=True)
    datetime = models.DateTimeField()
    def __str__(self):
        return self.team1.name + " vs " + self.team2.name + " " + self.datetime.strftime("%d/%m %Hh")

class Game(models.Model):
    user = models.ForeignKey("Person", on_delete=models.CASCADE)
    match = models.ForeignKey("Match", related_name="games", on_delete=models.CASCADE)
    firstHalfTeam1Score = models.IntegerField(null=True, blank=True)
    firstHalfTeam2Score = models.IntegerField(null=True, blank=True)
    fullTimeTeam1Score = models.IntegerField(null=True, blank=True)
    fullTimeTeam2Score = models.IntegerField(null=True, blank=True)
    dateTimeScoreCreated = models.DateTimeField()
    win = models.BooleanField(default=False)
    def __str__(self):
        return self.user.nickname + " for " + str(self.match)




