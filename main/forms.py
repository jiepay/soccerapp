from django import forms

class UserRegisterForm(forms.Form):
    nickname = forms.CharField(min_length=2,widget=forms.TextInput({'class':'form-control','placeholder':'Name', 'pattern': '.{2,}'}))
    phoneNumber = forms.CharField(max_length=8, min_length=7, widget=forms.TextInput({'class':'form-control','placeholder':'Phone Number', 'pattern': '.{7,8}'}))

    def clean(self):
        cleaned_data = super(UserRegisterForm, self).clean()
        self.nickname = cleaned_data.get('nickname')
        self.phonenumber = cleaned_data.get('phoneNumber')
        if not self.nickname and not self.phonenumber:
            raise forms.ValidationError('You have to write something!')


class GameForm(forms.Form):
    firstHalfTeam1Score = forms.IntegerField(min_value=0, widget=forms.TextInput({'class':'form-control','style':'margin-left:60%;width:40%;'}))
    firstHalfTeam2Score = forms.IntegerField(min_value=0, widget=forms.TextInput({'class':'form-control','style':'margin-right:50%;width:40%;'}))
    fullTimeTeam1Score = forms.IntegerField(min_value=0, widget=forms.TextInput({'class':'form-control','style':'margin-left:60%;width:40%;'}))
    fullTimeTeam2Score = forms.IntegerField(min_value=0, widget=forms.TextInput({'class':'form-control','style':'margin-right:50%;width:40%;'}))

    def clean(self):
        cleaned_data = super(GameForm, self).clean()
        self.firstHalfTeam1Score = cleaned_data.get('firstHalfTeam1Score')
        self.firstHalfTeam2Score = cleaned_data.get('firstHalfTeam2Score')
        self.fullTimeTeam1Score = cleaned_data.get('fullTimeTeam1Score')
        self.fullTimeTeam2Score = cleaned_data.get('fullTimeTeam2Score')

        if not self.fullTimeTeam1Score and not self.fullTimeTeam2Score:
            raise forms.ValidationError('You have to write something!')