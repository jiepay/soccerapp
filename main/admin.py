from django.contrib import admin
from django.utils import timezone
from pytz import timezone as tz
# Register your models here.
from django.contrib import admin
from .models import Team, Person, Match, Game

timezone.activate(tz('Indian/Mauritius'))
admin.site.register(Team)
admin.site.register(Person)
admin.site.register(Match)
admin.site.register(Game)