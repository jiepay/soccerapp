from django.urls import path
from . import views

urlpatterns = [
    #user views
    path('', views.index, name='index'),                        #login
    path('schedule', views.schedule, name='schedule'),          #view match schedule
    path('matchscore/<int:match_id>/', views.matchscore, name='matchscore_post'),
    # path('matchscore', views.matchscore, name='matchscore_get'),       #bet on matches
    path('thankyou', views.thankyou, name='thankyou'),
    path('logout', views.logout, name='logout'),
    #admin views
    # -- admin login, provided by django
    # -- admin match schedule modify, provided by django
    # -- admin enter correct score, provided by django
    path('winner', views.winner, name='winner'),                #view winners list
]